﻿Public Class Form1

    Dim choferes() As String = {"Lewis Hamilton", "Nico Rosberg", "Sebastian Vettel", "Kimi Räikkönen", "Fernando Alonso"}
    Dim auxiliares() As String = {"Michael Schumacher", "Juan Manuel Fangio", "Alain Prost", "Ayrton Senna", "Niki Lauda"}
    Dim rutchoferes() As String = {"1.111.111-1", "22.222.222-2", "3.333.333-3", "4.444.444-4", "5.555.555-6"}
    Dim rutauxiliares() As String = {"12.345.678-9", "2.543.876-1", "3.876.543-2", "4.582.473-k", "23.455.345-k"}
    Dim destino() As String = {"Concepción", "Curanilahue", "Los Alamos", "Lebu"}

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        For index = 0 To 4
            ComboBoxChofer.Items.Add(choferes(index))
            ComboBoxAuxiliar.Items.Add(auxiliares(index))
        Next

        For i = 0 To 3
            ComboBoxDestino.Items.Add(destino(i))
        Next

        ComboBoxChofer.Enabled = False
        ComboBoxAuxiliar.Enabled = False
        DateTimePickerDate.Enabled = False
        DateTimePickerTime.Enabled = False
        NumUDHasta100.Enabled = False
        NumUDHasta500.Enabled = False
        NumUDHasta1000.Enabled = False
        NumUDHasta5000.Enabled = False
        NumUDVentaConce.Enabled = False
        NumUDVentaChue.Enabled = False
        NumUDVentaLsAlamos.Enabled = False
        NumUDVentaLebu.Enabled = False
        NumUDPetroleo.Enabled = False
        NumUDGPeajes.Enabled = False
        NumUDOtros.Enabled = False

    End Sub
    Private Sub ComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxChofer.SelectedIndexChanged, ComboBoxAuxiliar.SelectedIndexChanged, ComboBoxDestino.SelectedIndexChanged, ComboBoxChofer.KeyPress, ComboBoxAuxiliar.KeyPress, ComboBoxDestino.KeyPress

        If ComboBoxChofer.SelectedText = "" AndAlso ComboBoxAuxiliar.SelectedText = "" AndAlso ComboBoxDestino.SelectedText = "" AndAlso Len(ComboBoxChofer.Text) <> 0 AndAlso Len(ComboBoxDestino.Text) <> 0 AndAlso Len(ComboBoxAuxiliar.Text) <> 0 Then
            NumUDHasta100.Enabled = True
            NumUDHasta500.Enabled = True
            NumUDHasta1000.Enabled = True
            NumUDHasta5000.Enabled = True
            NumUDVentaConce.Enabled = True
            NumUDVentaChue.Enabled = True
            NumUDVentaLsAlamos.Enabled = True
            NumUDVentaLebu.Enabled = True
            NumUDPetroleo.Enabled = True
            NumUDGPeajes.Enabled = True
            NumUDOtros.Enabled = True

        Else

            NumUDHasta100.Enabled = False
            NumUDHasta500.Enabled = False
            NumUDHasta1000.Enabled = False
            NumUDHasta5000.Enabled = False
            NumUDVentaConce.Enabled = False
            NumUDVentaChue.Enabled = False
            NumUDVentaLsAlamos.Enabled = False
            NumUDVentaLebu.Enabled = False
            NumUDPetroleo.Enabled = False
            NumUDGPeajes.Enabled = False
            NumUDOtros.Enabled = False

        End If
        'Dim destino As String = ComboBoxDestino.Text

        'Select Case destino
        '    Case "Concepción"
        '        NumUDVentaConce.Enabled = False
        '    Case "Curanilahue"
        '        NumUDVentaChue.Enabled = False
        '    Case "Los Alamos"
        '        NumUDVentaLsAlamos.Enabled = False
        '    Case "Lebu"
        '        NumUDVentaLebu.Enabled = False
        'End Select
    End Sub

    Private Sub ComboBoxChofer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxChofer.SelectedIndexChanged, ComboBoxChofer.KeyPress

        If ComboBoxChofer.SelectedText = "" AndAlso Len(ComboBoxChofer.Text) <> 0 Then

            TextBoxRutChofer.Text = rutchoferes(ComboBoxChofer.SelectedIndex)

        Else
            TextBoxRutChofer.Text = ""
        End If

    End Sub

    Private Sub ComboBoxAuxiliar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxAuxiliar.SelectedIndexChanged, ComboBoxAuxiliar.KeyPress

        If ComboBoxAuxiliar.SelectedText = "" AndAlso Len(ComboBoxAuxiliar.Text) <> 0 Then

            TextBoxRutAuxiliar.Text = rutauxiliares(ComboBoxAuxiliar.SelectedIndex)

        Else
            TextBoxRutAuxiliar.Text = ""
        End If

    End Sub

    Private Sub NumUD_Changed(sender As Object, e As EventArgs) Handles NumUDNumBus.ValueChanged, NumUDDesde100.ValueChanged, NumUDDesde500.ValueChanged, NumUDDesde1000.ValueChanged, NumUDDesde5000.ValueChanged, NumUDDesde100.KeyPress, NumUDDesde500.KeyPress, NumUDDesde1000.KeyPress, NumUDDesde5000.KeyPress, NumUDNumBus.KeyPress

        If NumUDDesde100.Text = "" Then
            NumUDDesde100.Text = 0
        End If

        If NumUDDesde500.Text = "" Then
            NumUDDesde500.Text = 0
        End If

        If NumUDDesde1000.Text = "" Then
            NumUDDesde100.Text = 0
        End If

        If NumUDDesde5000.Text = "" Then
            NumUDDesde5000.Text = 0
        End If

        If NumUDNumBus.Text = "" Then
            NumUDNumBus.Text = 0
        End If
        'Si esta establecido los numeros de pasaje iniciales y el numero de bus, se habilita la opcion de elegir chofer, auxiliar, fecha y hr de viaje
        If (NumUDNumBus.Value > 0 AndAlso NumUDDesde100.Value > 0 AndAlso NumUDDesde500.Value > 0 AndAlso NumUDDesde1000.Value > 0 AndAlso NumUDDesde5000.Value > 0) Then

            ComboBoxChofer.Enabled = True
            ComboBoxAuxiliar.Enabled = True
            DateTimePickerTime.Enabled = True
            DateTimePickerDate.Enabled = True

        Else
            ComboBoxChofer.Enabled = False
            ComboBoxAuxiliar.Enabled = False
            DateTimePickerTime.Enabled = False
            DateTimePickerDate.Enabled = False

        End If
    End Sub

    Private Sub NumUDPasajes_Changed(sender As Object, e As EventArgs) Handles NumUDHasta100.ValueChanged, NumUDHasta100.KeyPress, NumUDDesde100.ValueChanged, NumUDDesde100.KeyPress,
            NumUDHasta500.ValueChanged, NumUDHasta500.KeyPress, NumUDDesde500.ValueChanged, NumUDDesde500.KeyPress, NumUDHasta1000.ValueChanged, NumUDHasta1000.KeyPress, NumUDDesde1000.ValueChanged, NumUDDesde1000.KeyPress,
            NumUDHasta5000.ValueChanged, NumUDHasta5000.KeyPress, NumUDDesde5000.ValueChanged, NumUDDesde5000.KeyPress,
            NumUDVentaConce.ValueChanged, NumUDVentaChue.ValueChanged, NumUDVentaLsAlamos.ValueChanged, NumUDVentaLebu.ValueChanged, NumUDVentaConce.KeyPress, NumUDVentaChue.KeyPress, NumUDVentaLsAlamos.KeyPress, NumUDVentaLebu.KeyPress,
            NumUDPetroleo.ValueChanged, NumUDPetroleo.KeyPress, NumUDGPeajes.KeyPress, NumUDGPeajes.ValueChanged, NumUDOtros.ValueChanged, NumUDOtros.KeyPress

        If NumUDHasta100.Text = "" Then
            NumUDHasta100.Text = 0
        End If

        If NumUDHasta500.Text = "" Then
            NumUDHasta500.Text = 0
        End If

        If NumUDHasta1000.Text = "" Then
            NumUDHasta1000.Text = 0
        End If

        If NumUDHasta5000.Text = "" Then
            NumUDHasta5000.Text = 0
        End If

        If NumUDVentaConce.Text = "" Then
            NumUDVentaConce.Text = 0
        End If

        If NumUDVentaChue.Text = "" Then
            NumUDVentaChue.Text = 0
        End If

        If NumUDVentaLsAlamos.Text = "" Then
            NumUDVentaLsAlamos.Text = 0
        End If

        If NumUDVentaLebu.Text = "" Then
            NumUDVentaLebu.Text = 0
        End If

        If NumUDPetroleo.Text = "" Then
            NumUDPetroleo.Text = 0
        End If

        If NumUDGPeajes.Text = "" Then
            NumUDGPeajes.Text = 0
        End If

        If NumUDOtros.Text = "" Then
            NumUDOtros.Text = 0
        End If

        If NumUDHasta100.Value >= NumUDDesde100.Value AndAlso NumUDHasta100.Value > 0 Then
            TextBoxCanBol100.Text = NumUDHasta100.Value - NumUDDesde100.Value + 1
            TextBoxTotal100.Text = Convert.ToInt32(TextBoxCanBol100.Text) * 100
        Else
            TextBoxCanBol100.Text = 0
            TextBoxTotal100.Text = 0
        End If
        If NumUDHasta500.Value >= NumUDDesde500.Value AndAlso NumUDHasta500.Value > 0 Then
            TextBoxCanBol500.Text = NumUDHasta500.Value - NumUDDesde500.Value + 1
            TextBoxTotal500.Text = Convert.ToInt32(TextBoxCanBol500.Text) * 500
        Else
            TextBoxCanBol500.Text = 0
            TextBoxTotal500.Text = 0
        End If
        If NumUDHasta1000.Value >= NumUDDesde1000.Value AndAlso NumUDHasta1000.Value > 0 Then
            TextBoxCanBol1000.Text = NumUDHasta1000.Value - NumUDDesde1000.Value + 1
            TextBoxTotal1000.Text = Convert.ToInt32(TextBoxCanBol1000.Text) * 1000
        Else
            TextBoxCanBol1000.Text = 0
            TextBoxTotal1000.Text = 0
        End If
        If NumUDHasta5000.Value >= NumUDDesde5000.Value AndAlso NumUDHasta5000.Value > 0 Then
            TextBoxCanBol5000.Text = NumUDHasta5000.Value - NumUDDesde5000.Value + 1
            TextBoxTotal5000.Text = Convert.ToInt32(TextBoxCanBol5000.Text) * 5000
        Else
            TextBoxCanBol5000.Text = 0
            TextBoxTotal5000.Text = 0
        End If

        TextBoxVentasRuta.Text = Convert.ToInt32(TextBoxTotal100.Text) + Convert.ToInt32(TextBoxTotal500.Text) + Convert.ToInt32(TextBoxTotal1000.Text) + Convert.ToInt32(TextBoxTotal5000.Text)

        TextBoxTotalOf.Text = NumUDVentaConce.Value + NumUDVentaChue.Value + NumUDVentaLsAlamos.Value + NumUDVentaLebu.Value
        TextBoxVentasOficina.Text = TextBoxTotalOf.Text

        Dim porcertajeChofer As Double = (Convert.ToInt32(TextBoxVentasOficina.Text) + Convert.ToInt32(TextBoxVentasRuta.Text)) * 0.07
        Dim porcentajeAuxiliar As Double = (Convert.ToInt32(TextBoxVentasOficina.Text) + Convert.ToInt32(TextBoxVentasRuta.Text)) * 0.03

        TextBoxChofer.Text = Math.Round(porcertajeChofer)
        TextBoxAuxiliar.Text = Math.Round(porcentajeAuxiliar)

        TextBoxTotalGastos.Text = NumUDPetroleo.Value + NumUDGPeajes.Value + Convert.ToInt32(TextBoxChofer.Text) + Convert.ToInt32(TextBoxAuxiliar.Text) + NumUDOtros.Value
        TextBoxGastos.Text = TextBoxTotalGastos.Text

        Dim entregar As Integer = Convert.ToInt32(TextBoxVentasOficina.Text) + Convert.ToInt32(TextBoxVentasRuta.Text) - Convert.ToInt32(TextBoxTotalGastos.Text)

        If entregar >= 0 Then
            TextBoxEntregar.Text = entregar
        Else
            TextBoxEntregar.Text = 0
        End If

    End Sub

    Private Sub DateTimePickerTime_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePickerTime.ValueChanged, DateTimePickerTime.KeyPress
        DateTimePickerTime.MaxDate = DateTimePickerDate.Value
        DateTimePickerTime.MinDate = DateTimePickerDate.Value

    End Sub

End Class
