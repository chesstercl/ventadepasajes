﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.ComboBoxChofer = New System.Windows.Forms.ComboBox()
        Me.ComboBoxAuxiliar = New System.Windows.Forms.ComboBox()
        Me.LabelChofer = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LabelFechaSalida = New System.Windows.Forms.Label()
        Me.DateTimePickerDate = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePickerTime = New System.Windows.Forms.DateTimePicker()
        Me.LabelHorario = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxRutChofer = New System.Windows.Forms.TextBox()
        Me.TextBoxRutAuxiliar = New System.Windows.Forms.TextBox()
        Me.NumUDNumBus = New System.Windows.Forms.NumericUpDown()
        Me.LabelNumBus = New System.Windows.Forms.Label()
        Me.ComboBoxDestino = New System.Windows.Forms.ComboBox()
        Me.LabelDestino = New System.Windows.Forms.Label()
        Me.LabelVentaEnRuta = New System.Windows.Forms.Label()
        Me.LabelBoletos = New System.Windows.Forms.Label()
        Me.LabelDesde = New System.Windows.Forms.Label()
        Me.LabelHasta = New System.Windows.Forms.Label()
        Me.LabelCantBol = New System.Windows.Forms.Label()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.LabelConcepcion = New System.Windows.Forms.Label()
        Me.LabelCuranilahue = New System.Windows.Forms.Label()
        Me.LabelLosAlamos = New System.Windows.Forms.Label()
        Me.Lebu = New System.Windows.Forms.Label()
        Me.LabelTotalDeD = New System.Windows.Forms.Label()
        Me.LabelVentasOficina = New System.Windows.Forms.Label()
        Me.LabelVentaRuta = New System.Windows.Forms.Label()
        Me.LabelMenos = New System.Windows.Forms.Label()
        Me.LabelGastos = New System.Windows.Forms.Label()
        Me.LabelAEntregar = New System.Windows.Forms.Label()
        Me.LabelPetroleo = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBoxTotalOf = New System.Windows.Forms.TextBox()
        Me.NumUDVentaLebu = New System.Windows.Forms.NumericUpDown()
        Me.NumUDVentaLsAlamos = New System.Windows.Forms.NumericUpDown()
        Me.NumUDVentaChue = New System.Windows.Forms.NumericUpDown()
        Me.NumUDVentaConce = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.NumUDDesde5000 = New System.Windows.Forms.NumericUpDown()
        Me.NumUDDesde1000 = New System.Windows.Forms.NumericUpDown()
        Me.NumUDDesde500 = New System.Windows.Forms.NumericUpDown()
        Me.NumUDDesde100 = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.NumUDHasta5000 = New System.Windows.Forms.NumericUpDown()
        Me.NumUDHasta1000 = New System.Windows.Forms.NumericUpDown()
        Me.NumUDHasta500 = New System.Windows.Forms.NumericUpDown()
        Me.NumUDHasta100 = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextBoxCanBol100 = New System.Windows.Forms.TextBox()
        Me.TextBoxCanBol5000 = New System.Windows.Forms.TextBox()
        Me.TextBoxCanBol1000 = New System.Windows.Forms.TextBox()
        Me.TextBoxCanBol500 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBoxTotal100 = New System.Windows.Forms.TextBox()
        Me.TextBoxTotal5000 = New System.Windows.Forms.TextBox()
        Me.TextBoxTotal1000 = New System.Windows.Forms.TextBox()
        Me.TextBoxTotal500 = New System.Windows.Forms.TextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.TextBox5000 = New System.Windows.Forms.TextBox()
        Me.TextBox1000 = New System.Windows.Forms.TextBox()
        Me.TextBox500 = New System.Windows.Forms.TextBox()
        Me.TextBox100 = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextBoxTotalGastos = New System.Windows.Forms.TextBox()
        Me.TextBoxAuxiliar = New System.Windows.Forms.TextBox()
        Me.TextBoxChofer = New System.Windows.Forms.TextBox()
        Me.NumUDOtros = New System.Windows.Forms.NumericUpDown()
        Me.NumUDGPeajes = New System.Windows.Forms.NumericUpDown()
        Me.NumUDPetroleo = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.TextBoxEntregar = New System.Windows.Forms.TextBox()
        Me.TextBoxGastos = New System.Windows.Forms.TextBox()
        Me.TextBoxVentasRuta = New System.Windows.Forms.TextBox()
        Me.TextBoxVentasOficina = New System.Windows.Forms.TextBox()
        CType(Me.NumUDNumBus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumUDVentaLebu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDVentaLsAlamos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDVentaChue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDVentaConce, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.NumUDDesde5000, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDDesde1000, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDDesde500, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDDesde100, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.NumUDHasta5000, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDHasta1000, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDHasta500, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDHasta100, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.NumUDOtros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDGPeajes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumUDPetroleo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelTitle
        '
        Me.LabelTitle.AutoSize = True
        Me.LabelTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitle.Location = New System.Drawing.Point(169, 9)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(365, 20)
        Me.LabelTitle.TabIndex = 0
        Me.LabelTitle.Text = "Venta de Pasajes y registro de venta en ruta"
        '
        'ComboBoxChofer
        '
        Me.ComboBoxChofer.FormattingEnabled = True
        Me.ComboBoxChofer.Location = New System.Drawing.Point(129, 58)
        Me.ComboBoxChofer.Name = "ComboBoxChofer"
        Me.ComboBoxChofer.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxChofer.TabIndex = 1
        '
        'ComboBoxAuxiliar
        '
        Me.ComboBoxAuxiliar.FormattingEnabled = True
        Me.ComboBoxAuxiliar.Location = New System.Drawing.Point(129, 85)
        Me.ComboBoxAuxiliar.Name = "ComboBoxAuxiliar"
        Me.ComboBoxAuxiliar.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxAuxiliar.TabIndex = 2
        '
        'LabelChofer
        '
        Me.LabelChofer.AutoSize = True
        Me.LabelChofer.Location = New System.Drawing.Point(19, 63)
        Me.LabelChofer.Name = "LabelChofer"
        Me.LabelChofer.Size = New System.Drawing.Size(41, 13)
        Me.LabelChofer.TabIndex = 3
        Me.LabelChofer.Text = "Chofer:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Auxiliar:"
        '
        'LabelFechaSalida
        '
        Me.LabelFechaSalida.AutoSize = True
        Me.LabelFechaSalida.Location = New System.Drawing.Point(18, 122)
        Me.LabelFechaSalida.Name = "LabelFechaSalida"
        Me.LabelFechaSalida.Size = New System.Drawing.Size(72, 13)
        Me.LabelFechaSalida.TabIndex = 5
        Me.LabelFechaSalida.Text = "Fecha Salida:"
        '
        'DateTimePickerDate
        '
        Me.DateTimePickerDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePickerDate.Location = New System.Drawing.Point(129, 115)
        Me.DateTimePickerDate.Name = "DateTimePickerDate"
        Me.DateTimePickerDate.Size = New System.Drawing.Size(121, 20)
        Me.DateTimePickerDate.TabIndex = 6
        '
        'DateTimePickerTime
        '
        Me.DateTimePickerTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePickerTime.Location = New System.Drawing.Point(129, 142)
        Me.DateTimePickerTime.Name = "DateTimePickerTime"
        Me.DateTimePickerTime.Size = New System.Drawing.Size(121, 20)
        Me.DateTimePickerTime.TabIndex = 7
        '
        'LabelHorario
        '
        Me.LabelHorario.AutoSize = True
        Me.LabelHorario.Location = New System.Drawing.Point(16, 148)
        Me.LabelHorario.Name = "LabelHorario"
        Me.LabelHorario.Size = New System.Drawing.Size(41, 13)
        Me.LabelHorario.TabIndex = 8
        Me.LabelHorario.Text = "Horario"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(282, 65)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Rut:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(282, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(27, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Rut:"
        '
        'TextBoxRutChofer
        '
        Me.TextBoxRutChofer.Location = New System.Drawing.Point(357, 60)
        Me.TextBoxRutChofer.Name = "TextBoxRutChofer"
        Me.TextBoxRutChofer.ReadOnly = True
        Me.TextBoxRutChofer.Size = New System.Drawing.Size(121, 20)
        Me.TextBoxRutChofer.TabIndex = 13
        '
        'TextBoxRutAuxiliar
        '
        Me.TextBoxRutAuxiliar.Location = New System.Drawing.Point(357, 86)
        Me.TextBoxRutAuxiliar.Name = "TextBoxRutAuxiliar"
        Me.TextBoxRutAuxiliar.ReadOnly = True
        Me.TextBoxRutAuxiliar.Size = New System.Drawing.Size(121, 20)
        Me.TextBoxRutAuxiliar.TabIndex = 14
        '
        'NumUDNumBus
        '
        Me.NumUDNumBus.Location = New System.Drawing.Point(357, 115)
        Me.NumUDNumBus.Name = "NumUDNumBus"
        Me.NumUDNumBus.Size = New System.Drawing.Size(121, 20)
        Me.NumUDNumBus.TabIndex = 15
        '
        'LabelNumBus
        '
        Me.LabelNumBus.AutoSize = True
        Me.LabelNumBus.Location = New System.Drawing.Point(282, 122)
        Me.LabelNumBus.Name = "LabelNumBus"
        Me.LabelNumBus.Size = New System.Drawing.Size(45, 13)
        Me.LabelNumBus.TabIndex = 16
        Me.LabelNumBus.Text = "numBus"
        '
        'ComboBoxDestino
        '
        Me.ComboBoxDestino.FormattingEnabled = True
        Me.ComboBoxDestino.Location = New System.Drawing.Point(357, 140)
        Me.ComboBoxDestino.Name = "ComboBoxDestino"
        Me.ComboBoxDestino.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxDestino.TabIndex = 17
        '
        'LabelDestino
        '
        Me.LabelDestino.AutoSize = True
        Me.LabelDestino.Location = New System.Drawing.Point(282, 148)
        Me.LabelDestino.Name = "LabelDestino"
        Me.LabelDestino.Size = New System.Drawing.Size(43, 13)
        Me.LabelDestino.TabIndex = 18
        Me.LabelDestino.Text = "Destino"
        '
        'LabelVentaEnRuta
        '
        Me.LabelVentaEnRuta.AutoSize = True
        Me.LabelVentaEnRuta.Location = New System.Drawing.Point(20, 319)
        Me.LabelVentaEnRuta.Name = "LabelVentaEnRuta"
        Me.LabelVentaEnRuta.Size = New System.Drawing.Size(71, 13)
        Me.LabelVentaEnRuta.TabIndex = 19
        Me.LabelVentaEnRuta.Text = "Venta en ruta"
        '
        'LabelBoletos
        '
        Me.LabelBoletos.AutoSize = True
        Me.LabelBoletos.Location = New System.Drawing.Point(9, 20)
        Me.LabelBoletos.Name = "LabelBoletos"
        Me.LabelBoletos.Size = New System.Drawing.Size(41, 13)
        Me.LabelBoletos.TabIndex = 20
        Me.LabelBoletos.Text = "boletos"
        '
        'LabelDesde
        '
        Me.LabelDesde.AutoSize = True
        Me.LabelDesde.Location = New System.Drawing.Point(9, 20)
        Me.LabelDesde.Name = "LabelDesde"
        Me.LabelDesde.Size = New System.Drawing.Size(38, 13)
        Me.LabelDesde.TabIndex = 25
        Me.LabelDesde.Text = "Desde"
        '
        'LabelHasta
        '
        Me.LabelHasta.AutoSize = True
        Me.LabelHasta.Location = New System.Drawing.Point(9, 25)
        Me.LabelHasta.Name = "LabelHasta"
        Me.LabelHasta.Size = New System.Drawing.Size(35, 13)
        Me.LabelHasta.TabIndex = 26
        Me.LabelHasta.Text = "Hasta"
        '
        'LabelCantBol
        '
        Me.LabelCantBol.AutoSize = True
        Me.LabelCantBol.Location = New System.Drawing.Point(9, 20)
        Me.LabelCantBol.Name = "LabelCantBol"
        Me.LabelCantBol.Size = New System.Drawing.Size(45, 13)
        Me.LabelCantBol.TabIndex = 27
        Me.LabelCantBol.Text = "cant bol"
        '
        'LabelTotal
        '
        Me.LabelTotal.AutoSize = True
        Me.LabelTotal.Location = New System.Drawing.Point(8, 20)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(36, 13)
        Me.LabelTotal.TabIndex = 28
        Me.LabelTotal.Text = "total $"
        '
        'LabelConcepcion
        '
        Me.LabelConcepcion.AutoSize = True
        Me.LabelConcepcion.Location = New System.Drawing.Point(11, 28)
        Me.LabelConcepcion.Name = "LabelConcepcion"
        Me.LabelConcepcion.Size = New System.Drawing.Size(64, 13)
        Me.LabelConcepcion.TabIndex = 30
        Me.LabelConcepcion.Text = "Concepcion"
        '
        'LabelCuranilahue
        '
        Me.LabelCuranilahue.AutoSize = True
        Me.LabelCuranilahue.Location = New System.Drawing.Point(11, 54)
        Me.LabelCuranilahue.Name = "LabelCuranilahue"
        Me.LabelCuranilahue.Size = New System.Drawing.Size(63, 13)
        Me.LabelCuranilahue.TabIndex = 31
        Me.LabelCuranilahue.Text = "Curanilahue"
        '
        'LabelLosAlamos
        '
        Me.LabelLosAlamos.AutoSize = True
        Me.LabelLosAlamos.Location = New System.Drawing.Point(8, 78)
        Me.LabelLosAlamos.Name = "LabelLosAlamos"
        Me.LabelLosAlamos.Size = New System.Drawing.Size(61, 13)
        Me.LabelLosAlamos.TabIndex = 32
        Me.LabelLosAlamos.Text = "Los Alamos"
        '
        'Lebu
        '
        Me.Lebu.AutoSize = True
        Me.Lebu.Location = New System.Drawing.Point(10, 104)
        Me.Lebu.Name = "Lebu"
        Me.Lebu.Size = New System.Drawing.Size(31, 13)
        Me.Lebu.TabIndex = 33
        Me.Lebu.Text = "Lebu"
        '
        'LabelTotalDeD
        '
        Me.LabelTotalDeD.AutoSize = True
        Me.LabelTotalDeD.Location = New System.Drawing.Point(9, 125)
        Me.LabelTotalDeD.Name = "LabelTotalDeD"
        Me.LabelTotalDeD.Size = New System.Drawing.Size(55, 13)
        Me.LabelTotalDeD.TabIndex = 34
        Me.LabelTotalDeD.Text = "Total de $"
        '
        'LabelVentasOficina
        '
        Me.LabelVentasOficina.AutoSize = True
        Me.LabelVentasOficina.Location = New System.Drawing.Point(6, 23)
        Me.LabelVentasOficina.Name = "LabelVentasOficina"
        Me.LabelVentasOficina.Size = New System.Drawing.Size(75, 13)
        Me.LabelVentasOficina.TabIndex = 36
        Me.LabelVentasOficina.Text = "ventas Oficina"
        '
        'LabelVentaRuta
        '
        Me.LabelVentaRuta.AutoSize = True
        Me.LabelVentaRuta.Location = New System.Drawing.Point(6, 49)
        Me.LabelVentaRuta.Name = "LabelVentaRuta"
        Me.LabelVentaRuta.Size = New System.Drawing.Size(75, 13)
        Me.LabelVentaRuta.TabIndex = 37
        Me.LabelVentaRuta.Text = "venta en Ruta"
        '
        'LabelMenos
        '
        Me.LabelMenos.AutoSize = True
        Me.LabelMenos.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMenos.Location = New System.Drawing.Point(7, 74)
        Me.LabelMenos.Name = "LabelMenos"
        Me.LabelMenos.Size = New System.Drawing.Size(33, 12)
        Me.LabelMenos.TabIndex = 38
        Me.LabelMenos.Text = "menos"
        '
        'LabelGastos
        '
        Me.LabelGastos.AutoSize = True
        Me.LabelGastos.Location = New System.Drawing.Point(6, 97)
        Me.LabelGastos.Name = "LabelGastos"
        Me.LabelGastos.Size = New System.Drawing.Size(40, 13)
        Me.LabelGastos.TabIndex = 39
        Me.LabelGastos.Text = "Gastos"
        '
        'LabelAEntregar
        '
        Me.LabelAEntregar.AutoSize = True
        Me.LabelAEntregar.Location = New System.Drawing.Point(6, 123)
        Me.LabelAEntregar.Name = "LabelAEntregar"
        Me.LabelAEntregar.Size = New System.Drawing.Size(55, 13)
        Me.LabelAEntregar.TabIndex = 40
        Me.LabelAEntregar.Text = "a entregar"
        '
        'LabelPetroleo
        '
        Me.LabelPetroleo.AutoSize = True
        Me.LabelPetroleo.Location = New System.Drawing.Point(6, 28)
        Me.LabelPetroleo.Name = "LabelPetroleo"
        Me.LabelPetroleo.Size = New System.Drawing.Size(46, 13)
        Me.LabelPetroleo.TabIndex = 41
        Me.LabelPetroleo.Text = "Petroleo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Peajes"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 78)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "7% Chofer"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "3% Auxiliar"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 130)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 13)
        Me.Label8.TabIndex = 46
        Me.Label8.Text = "otros"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 157)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 13)
        Me.Label9.TabIndex = 47
        Me.Label9.Text = "Total Gastos"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBoxTotalOf)
        Me.GroupBox1.Controls.Add(Me.NumUDVentaLebu)
        Me.GroupBox1.Controls.Add(Me.NumUDVentaLsAlamos)
        Me.GroupBox1.Controls.Add(Me.NumUDVentaChue)
        Me.GroupBox1.Controls.Add(Me.NumUDVentaConce)
        Me.GroupBox1.Controls.Add(Me.LabelCuranilahue)
        Me.GroupBox1.Controls.Add(Me.LabelConcepcion)
        Me.GroupBox1.Controls.Add(Me.LabelLosAlamos)
        Me.GroupBox1.Controls.Add(Me.Lebu)
        Me.GroupBox1.Controls.Add(Me.LabelTotalDeD)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 488)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(213, 175)
        Me.GroupBox1.TabIndex = 48
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ventas de Oficina"
        '
        'TextBoxTotalOf
        '
        Me.TextBoxTotalOf.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTotalOf.Location = New System.Drawing.Point(124, 122)
        Me.TextBoxTotalOf.Name = "TextBoxTotalOf"
        Me.TextBoxTotalOf.ReadOnly = True
        Me.TextBoxTotalOf.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxTotalOf.TabIndex = 36
        '
        'NumUDVentaLebu
        '
        Me.NumUDVentaLebu.Location = New System.Drawing.Point(125, 97)
        Me.NumUDVentaLebu.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDVentaLebu.Name = "NumUDVentaLebu"
        Me.NumUDVentaLebu.Size = New System.Drawing.Size(67, 20)
        Me.NumUDVentaLebu.TabIndex = 37
        Me.NumUDVentaLebu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDVentaLsAlamos
        '
        Me.NumUDVentaLsAlamos.Location = New System.Drawing.Point(125, 71)
        Me.NumUDVentaLsAlamos.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDVentaLsAlamos.Name = "NumUDVentaLsAlamos"
        Me.NumUDVentaLsAlamos.Size = New System.Drawing.Size(67, 20)
        Me.NumUDVentaLsAlamos.TabIndex = 36
        Me.NumUDVentaLsAlamos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDVentaChue
        '
        Me.NumUDVentaChue.Location = New System.Drawing.Point(124, 47)
        Me.NumUDVentaChue.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDVentaChue.Name = "NumUDVentaChue"
        Me.NumUDVentaChue.Size = New System.Drawing.Size(67, 20)
        Me.NumUDVentaChue.TabIndex = 35
        Me.NumUDVentaChue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDVentaConce
        '
        Me.NumUDVentaConce.Location = New System.Drawing.Point(124, 21)
        Me.NumUDVentaConce.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDVentaConce.Name = "NumUDVentaConce"
        Me.NumUDVentaConce.Size = New System.Drawing.Size(67, 20)
        Me.NumUDVentaConce.TabIndex = 30
        Me.NumUDVentaConce.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.NumUDDesde5000)
        Me.GroupBox4.Controls.Add(Me.NumUDDesde1000)
        Me.GroupBox4.Controls.Add(Me.NumUDDesde500)
        Me.GroupBox4.Controls.Add(Me.NumUDDesde100)
        Me.GroupBox4.Controls.Add(Me.LabelDesde)
        Me.GroupBox4.Location = New System.Drawing.Point(129, 244)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(376, 51)
        Me.GroupBox4.TabIndex = 25
        Me.GroupBox4.TabStop = False
        '
        'NumUDDesde5000
        '
        Me.NumUDDesde5000.Location = New System.Drawing.Point(282, 18)
        Me.NumUDDesde5000.Name = "NumUDDesde5000"
        Me.NumUDDesde5000.Size = New System.Drawing.Size(67, 20)
        Me.NumUDDesde5000.TabIndex = 29
        Me.NumUDDesde5000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDDesde1000
        '
        Me.NumUDDesde1000.Location = New System.Drawing.Point(206, 18)
        Me.NumUDDesde1000.Name = "NumUDDesde1000"
        Me.NumUDDesde1000.Size = New System.Drawing.Size(67, 20)
        Me.NumUDDesde1000.TabIndex = 28
        Me.NumUDDesde1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDDesde500
        '
        Me.NumUDDesde500.Location = New System.Drawing.Point(131, 19)
        Me.NumUDDesde500.Name = "NumUDDesde500"
        Me.NumUDDesde500.Size = New System.Drawing.Size(67, 20)
        Me.NumUDDesde500.TabIndex = 27
        Me.NumUDDesde500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDDesde100
        '
        Me.NumUDDesde100.Location = New System.Drawing.Point(58, 18)
        Me.NumUDDesde100.Name = "NumUDDesde100"
        Me.NumUDDesde100.Size = New System.Drawing.Size(67, 20)
        Me.NumUDDesde100.TabIndex = 26
        Me.NumUDDesde100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.NumUDHasta5000)
        Me.GroupBox5.Controls.Add(Me.NumUDHasta1000)
        Me.GroupBox5.Controls.Add(Me.NumUDHasta500)
        Me.GroupBox5.Controls.Add(Me.NumUDHasta100)
        Me.GroupBox5.Controls.Add(Me.LabelHasta)
        Me.GroupBox5.Location = New System.Drawing.Point(129, 301)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(376, 51)
        Me.GroupBox5.TabIndex = 30
        Me.GroupBox5.TabStop = False
        '
        'NumUDHasta5000
        '
        Me.NumUDHasta5000.Location = New System.Drawing.Point(282, 18)
        Me.NumUDHasta5000.Name = "NumUDHasta5000"
        Me.NumUDHasta5000.Size = New System.Drawing.Size(67, 20)
        Me.NumUDHasta5000.TabIndex = 29
        Me.NumUDHasta5000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDHasta1000
        '
        Me.NumUDHasta1000.Location = New System.Drawing.Point(206, 16)
        Me.NumUDHasta1000.Name = "NumUDHasta1000"
        Me.NumUDHasta1000.Size = New System.Drawing.Size(67, 20)
        Me.NumUDHasta1000.TabIndex = 28
        Me.NumUDHasta1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDHasta500
        '
        Me.NumUDHasta500.Location = New System.Drawing.Point(131, 18)
        Me.NumUDHasta500.Name = "NumUDHasta500"
        Me.NumUDHasta500.Size = New System.Drawing.Size(67, 20)
        Me.NumUDHasta500.TabIndex = 27
        Me.NumUDHasta500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDHasta100
        '
        Me.NumUDHasta100.Location = New System.Drawing.Point(58, 16)
        Me.NumUDHasta100.Name = "NumUDHasta100"
        Me.NumUDHasta100.Size = New System.Drawing.Size(67, 20)
        Me.NumUDHasta100.TabIndex = 26
        Me.NumUDHasta100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextBoxCanBol100)
        Me.GroupBox6.Controls.Add(Me.TextBoxCanBol5000)
        Me.GroupBox6.Controls.Add(Me.TextBoxCanBol1000)
        Me.GroupBox6.Controls.Add(Me.TextBoxCanBol500)
        Me.GroupBox6.Controls.Add(Me.LabelCantBol)
        Me.GroupBox6.Location = New System.Drawing.Point(129, 358)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(376, 51)
        Me.GroupBox6.TabIndex = 31
        Me.GroupBox6.TabStop = False
        '
        'TextBoxCanBol100
        '
        Me.TextBoxCanBol100.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxCanBol100.Location = New System.Drawing.Point(58, 17)
        Me.TextBoxCanBol100.Name = "TextBoxCanBol100"
        Me.TextBoxCanBol100.ReadOnly = True
        Me.TextBoxCanBol100.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxCanBol100.TabIndex = 32
        '
        'TextBoxCanBol5000
        '
        Me.TextBoxCanBol5000.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxCanBol5000.Location = New System.Drawing.Point(282, 17)
        Me.TextBoxCanBol5000.Name = "TextBoxCanBol5000"
        Me.TextBoxCanBol5000.ReadOnly = True
        Me.TextBoxCanBol5000.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxCanBol5000.TabIndex = 31
        '
        'TextBoxCanBol1000
        '
        Me.TextBoxCanBol1000.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxCanBol1000.Location = New System.Drawing.Point(206, 17)
        Me.TextBoxCanBol1000.Name = "TextBoxCanBol1000"
        Me.TextBoxCanBol1000.ReadOnly = True
        Me.TextBoxCanBol1000.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxCanBol1000.TabIndex = 30
        '
        'TextBoxCanBol500
        '
        Me.TextBoxCanBol500.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxCanBol500.Location = New System.Drawing.Point(131, 17)
        Me.TextBoxCanBol500.Name = "TextBoxCanBol500"
        Me.TextBoxCanBol500.ReadOnly = True
        Me.TextBoxCanBol500.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxCanBol500.TabIndex = 29
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBoxTotal100)
        Me.GroupBox2.Controls.Add(Me.TextBoxTotal5000)
        Me.GroupBox2.Controls.Add(Me.LabelTotal)
        Me.GroupBox2.Controls.Add(Me.TextBoxTotal1000)
        Me.GroupBox2.Controls.Add(Me.TextBoxTotal500)
        Me.GroupBox2.Location = New System.Drawing.Point(129, 420)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(376, 51)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        '
        'TextBoxTotal100
        '
        Me.TextBoxTotal100.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTotal100.Location = New System.Drawing.Point(58, 17)
        Me.TextBoxTotal100.Name = "TextBoxTotal100"
        Me.TextBoxTotal100.ReadOnly = True
        Me.TextBoxTotal100.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxTotal100.TabIndex = 33
        '
        'TextBoxTotal5000
        '
        Me.TextBoxTotal5000.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTotal5000.Location = New System.Drawing.Point(282, 17)
        Me.TextBoxTotal5000.Name = "TextBoxTotal5000"
        Me.TextBoxTotal5000.ReadOnly = True
        Me.TextBoxTotal5000.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxTotal5000.TabIndex = 35
        '
        'TextBoxTotal1000
        '
        Me.TextBoxTotal1000.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTotal1000.Location = New System.Drawing.Point(206, 17)
        Me.TextBoxTotal1000.Name = "TextBoxTotal1000"
        Me.TextBoxTotal1000.ReadOnly = True
        Me.TextBoxTotal1000.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxTotal1000.TabIndex = 34
        '
        'TextBoxTotal500
        '
        Me.TextBoxTotal500.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTotal500.Location = New System.Drawing.Point(131, 17)
        Me.TextBoxTotal500.Name = "TextBoxTotal500"
        Me.TextBoxTotal500.ReadOnly = True
        Me.TextBoxTotal500.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxTotal500.TabIndex = 33
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.TextBox5000)
        Me.GroupBox7.Controls.Add(Me.TextBox1000)
        Me.GroupBox7.Controls.Add(Me.TextBox500)
        Me.GroupBox7.Controls.Add(Me.TextBox100)
        Me.GroupBox7.Controls.Add(Me.LabelBoletos)
        Me.GroupBox7.Location = New System.Drawing.Point(129, 189)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(376, 51)
        Me.GroupBox7.TabIndex = 33
        Me.GroupBox7.TabStop = False
        '
        'TextBox5000
        '
        Me.TextBox5000.Location = New System.Drawing.Point(282, 19)
        Me.TextBox5000.Name = "TextBox5000"
        Me.TextBox5000.ReadOnly = True
        Me.TextBox5000.Size = New System.Drawing.Size(67, 20)
        Me.TextBox5000.TabIndex = 52
        Me.TextBox5000.Text = "5000"
        Me.TextBox5000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox1000
        '
        Me.TextBox1000.Location = New System.Drawing.Point(206, 19)
        Me.TextBox1000.Name = "TextBox1000"
        Me.TextBox1000.ReadOnly = True
        Me.TextBox1000.Size = New System.Drawing.Size(67, 20)
        Me.TextBox1000.TabIndex = 51
        Me.TextBox1000.Text = "1000"
        Me.TextBox1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox500
        '
        Me.TextBox500.Location = New System.Drawing.Point(131, 19)
        Me.TextBox500.Name = "TextBox500"
        Me.TextBox500.ReadOnly = True
        Me.TextBox500.Size = New System.Drawing.Size(67, 20)
        Me.TextBox500.TabIndex = 50
        Me.TextBox500.Text = "500"
        Me.TextBox500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox100
        '
        Me.TextBox100.Location = New System.Drawing.Point(58, 19)
        Me.TextBox100.Name = "TextBox100"
        Me.TextBox100.ReadOnly = True
        Me.TextBox100.Size = New System.Drawing.Size(67, 20)
        Me.TextBox100.TabIndex = 49
        Me.TextBox100.Text = "100"
        Me.TextBox100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBoxTotalGastos)
        Me.GroupBox3.Controls.Add(Me.TextBoxAuxiliar)
        Me.GroupBox3.Controls.Add(Me.TextBoxChofer)
        Me.GroupBox3.Controls.Add(Me.NumUDOtros)
        Me.GroupBox3.Controls.Add(Me.NumUDGPeajes)
        Me.GroupBox3.Controls.Add(Me.NumUDPetroleo)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.LabelPetroleo)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Location = New System.Drawing.Point(265, 488)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.GroupBox3.Size = New System.Drawing.Size(213, 203)
        Me.GroupBox3.TabIndex = 49
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "   Gastos"
        '
        'TextBoxTotalGastos
        '
        Me.TextBoxTotalGastos.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTotalGastos.Location = New System.Drawing.Point(124, 150)
        Me.TextBoxTotalGastos.Name = "TextBoxTotalGastos"
        Me.TextBoxTotalGastos.ReadOnly = True
        Me.TextBoxTotalGastos.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxTotalGastos.TabIndex = 49
        '
        'TextBoxAuxiliar
        '
        Me.TextBoxAuxiliar.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxAuxiliar.Location = New System.Drawing.Point(124, 97)
        Me.TextBoxAuxiliar.Name = "TextBoxAuxiliar"
        Me.TextBoxAuxiliar.ReadOnly = True
        Me.TextBoxAuxiliar.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxAuxiliar.TabIndex = 48
        '
        'TextBoxChofer
        '
        Me.TextBoxChofer.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxChofer.Location = New System.Drawing.Point(124, 74)
        Me.TextBoxChofer.Name = "TextBoxChofer"
        Me.TextBoxChofer.ReadOnly = True
        Me.TextBoxChofer.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxChofer.TabIndex = 38
        '
        'NumUDOtros
        '
        Me.NumUDOtros.Location = New System.Drawing.Point(125, 123)
        Me.NumUDOtros.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDOtros.Name = "NumUDOtros"
        Me.NumUDOtros.Size = New System.Drawing.Size(67, 20)
        Me.NumUDOtros.TabIndex = 38
        Me.NumUDOtros.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDGPeajes
        '
        Me.NumUDGPeajes.Location = New System.Drawing.Point(124, 47)
        Me.NumUDGPeajes.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDGPeajes.Name = "NumUDGPeajes"
        Me.NumUDGPeajes.Size = New System.Drawing.Size(67, 20)
        Me.NumUDGPeajes.TabIndex = 35
        Me.NumUDGPeajes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'NumUDPetroleo
        '
        Me.NumUDPetroleo.Location = New System.Drawing.Point(124, 21)
        Me.NumUDPetroleo.Maximum = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me.NumUDPetroleo.Name = "NumUDPetroleo"
        Me.NumUDPetroleo.Size = New System.Drawing.Size(67, 20)
        Me.NumUDPetroleo.TabIndex = 30
        Me.NumUDPetroleo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.TextBoxEntregar)
        Me.GroupBox8.Controls.Add(Me.TextBoxGastos)
        Me.GroupBox8.Controls.Add(Me.TextBoxVentasRuta)
        Me.GroupBox8.Controls.Add(Me.TextBoxVentasOficina)
        Me.GroupBox8.Controls.Add(Me.LabelVentasOficina)
        Me.GroupBox8.Controls.Add(Me.LabelVentaRuta)
        Me.GroupBox8.Controls.Add(Me.LabelGastos)
        Me.GroupBox8.Controls.Add(Me.LabelAEntregar)
        Me.GroupBox8.Controls.Add(Me.LabelMenos)
        Me.GroupBox8.Location = New System.Drawing.Point(529, 488)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(213, 175)
        Me.GroupBox8.TabIndex = 49
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Resumen"
        '
        'TextBoxEntregar
        '
        Me.TextBoxEntregar.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxEntregar.Location = New System.Drawing.Point(122, 118)
        Me.TextBoxEntregar.Name = "TextBoxEntregar"
        Me.TextBoxEntregar.ReadOnly = True
        Me.TextBoxEntregar.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxEntregar.TabIndex = 53
        '
        'TextBoxGastos
        '
        Me.TextBoxGastos.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxGastos.Location = New System.Drawing.Point(122, 94)
        Me.TextBoxGastos.Name = "TextBoxGastos"
        Me.TextBoxGastos.ReadOnly = True
        Me.TextBoxGastos.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxGastos.TabIndex = 52
        '
        'TextBoxVentasRuta
        '
        Me.TextBoxVentasRuta.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxVentasRuta.Location = New System.Drawing.Point(122, 49)
        Me.TextBoxVentasRuta.Name = "TextBoxVentasRuta"
        Me.TextBoxVentasRuta.ReadOnly = True
        Me.TextBoxVentasRuta.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxVentasRuta.TabIndex = 51
        '
        'TextBoxVentasOficina
        '
        Me.TextBoxVentasOficina.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxVentasOficina.Location = New System.Drawing.Point(122, 23)
        Me.TextBoxVentasOficina.Name = "TextBoxVentasOficina"
        Me.TextBoxVentasOficina.ReadOnly = True
        Me.TextBoxVentasOficina.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxVentasOficina.TabIndex = 50
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(891, 661)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LabelVentaEnRuta)
        Me.Controls.Add(Me.LabelDestino)
        Me.Controls.Add(Me.ComboBoxDestino)
        Me.Controls.Add(Me.LabelNumBus)
        Me.Controls.Add(Me.NumUDNumBus)
        Me.Controls.Add(Me.TextBoxRutAuxiliar)
        Me.Controls.Add(Me.TextBoxRutChofer)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LabelHorario)
        Me.Controls.Add(Me.DateTimePickerTime)
        Me.Controls.Add(Me.DateTimePickerDate)
        Me.Controls.Add(Me.LabelFechaSalida)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LabelChofer)
        Me.Controls.Add(Me.ComboBoxAuxiliar)
        Me.Controls.Add(Me.ComboBoxChofer)
        Me.Controls.Add(Me.LabelTitle)
        Me.Name = "Form1"
        Me.Text = "Venta de Pasajes y registro de venta en ruta"
        CType(Me.NumUDNumBus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumUDVentaLebu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDVentaLsAlamos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDVentaChue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDVentaConce, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.NumUDDesde5000, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDDesde1000, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDDesde500, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDDesde100, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.NumUDHasta5000, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDHasta1000, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDHasta500, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDHasta100, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.NumUDOtros, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDGPeajes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumUDPetroleo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents ComboBoxChofer As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxAuxiliar As System.Windows.Forms.ComboBox
    Friend WithEvents LabelChofer As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LabelFechaSalida As System.Windows.Forms.Label
    Friend WithEvents DateTimePickerDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePickerTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents LabelHorario As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBoxRutChofer As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxRutAuxiliar As System.Windows.Forms.TextBox
    Friend WithEvents NumUDNumBus As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelNumBus As System.Windows.Forms.Label
    Friend WithEvents ComboBoxDestino As System.Windows.Forms.ComboBox
    Friend WithEvents LabelDestino As System.Windows.Forms.Label
    Friend WithEvents LabelVentaEnRuta As System.Windows.Forms.Label
    Friend WithEvents LabelBoletos As System.Windows.Forms.Label
    Friend WithEvents LabelDesde As System.Windows.Forms.Label
    Friend WithEvents LabelHasta As System.Windows.Forms.Label
    Friend WithEvents LabelCantBol As System.Windows.Forms.Label
    Friend WithEvents LabelTotal As System.Windows.Forms.Label
    Friend WithEvents LabelConcepcion As System.Windows.Forms.Label
    Friend WithEvents LabelCuranilahue As System.Windows.Forms.Label
    Friend WithEvents LabelLosAlamos As System.Windows.Forms.Label
    Friend WithEvents Lebu As System.Windows.Forms.Label
    Friend WithEvents LabelTotalDeD As System.Windows.Forms.Label
    Friend WithEvents LabelVentasOficina As System.Windows.Forms.Label
    Friend WithEvents LabelVentaRuta As System.Windows.Forms.Label
    Friend WithEvents LabelMenos As System.Windows.Forms.Label
    Friend WithEvents LabelGastos As System.Windows.Forms.Label
    Friend WithEvents LabelAEntregar As System.Windows.Forms.Label
    Friend WithEvents LabelPetroleo As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents NumUDDesde5000 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDDesde1000 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDDesde500 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDDesde100 As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents NumUDHasta5000 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDHasta1000 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDHasta500 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDHasta100 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDVentaLebu As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDVentaLsAlamos As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDVentaChue As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDVentaConce As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox5000 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1000 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox500 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox100 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents NumUDOtros As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDGPeajes As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumUDPetroleo As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxTotalOf As TextBox
    Friend WithEvents TextBoxCanBol5000 As TextBox
    Friend WithEvents TextBoxCanBol1000 As TextBox
    Friend WithEvents TextBoxCanBol500 As TextBox
    Friend WithEvents TextBoxTotal5000 As TextBox
    Friend WithEvents TextBoxTotal1000 As TextBox
    Friend WithEvents TextBoxTotal500 As TextBox
    Friend WithEvents TextBoxTotalGastos As TextBox
    Friend WithEvents TextBoxAuxiliar As TextBox
    Friend WithEvents TextBoxChofer As TextBox
    Friend WithEvents TextBoxEntregar As TextBox
    Friend WithEvents TextBoxGastos As TextBox
    Friend WithEvents TextBoxVentasRuta As TextBox
    Friend WithEvents TextBoxVentasOficina As TextBox
    Friend WithEvents TextBoxCanBol100 As TextBox
    Friend WithEvents TextBoxTotal100 As TextBox
End Class
